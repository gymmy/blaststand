﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Input;

public class BulletController : MonoBehaviour {

	[SerializeField]
	private GameObject bulletPrefab;

	private void SpawnBullet(Vector3 pSpawnPosition, float pForceAngle)
	{
		Vector3 dir = Quaternion.AngleAxis(pForceAngle, pSpawnPosition) * Vector3.right;

		GameObject newBullet = Instantiate(bulletPrefab, transform);
		newBullet.transform.position = pSpawnPosition;
		newBullet.GetComponent<Rigidbody2D>().AddForce(dir);
	}
}
