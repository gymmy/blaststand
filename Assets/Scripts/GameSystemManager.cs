﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Events;

public class StartEvent : GameEvent {
	public StartEvent(object author, bool shouldRecord = true) : base(author, shouldRecord) { }
}

/* GameSystemManager
 * Desc: In charge of managing custom game events like Start() and Update()
 */
[DisallowMultipleComponent]
public sealed class GameSystemManager : MonoBehaviour {
	private void Start() {
		new StartEvent(this);
	}
}