﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Events {
	/* GameEvent
	 * Desc: Abstract base class for Events, event raised might be important to keep track later on as Author member
	 */
	[System.Serializable]
	public abstract class GameEvent {
		public readonly object Author;
		private readonly bool _shouldRecord;

		public GameEvent(object author, bool shouldRecord = true)
		{
			Author = author;
			_shouldRecord = shouldRecord;
			Raise();
		}

		private void Raise() {
			if (_shouldRecord) {
				GameEventSystem.RecordEvent(this);
			}
		}
	}

	/* GameEventSystem
	 * Desc: Should be used for single call events that might be important to keep track later on (I.E. instances where Achievements is tied) 
	 */
	public static class GameEventSystem {
		private static List<GameEvent> _gameEvents = new List<GameEvent>();

		public static void RecordEvent<T>(T gameEvent) where T : GameEvent {
			_gameEvents.Add(gameEvent);
			Debug.Log("Event: " + typeof(T).ToString() + " RecordCount: " + _gameEvents.Count);
		}

		public static T ReadEvent<T>() where T : GameEvent {
			T query = null;
			query = _gameEvents.Where(q=>q.GetType() == typeof(T)).FirstOrDefault() as T;
			return query;
		}
	}
}